package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/dig_zap/grpc-poc/bidirectional/pb"
	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Client for streaming server starting!")

	/* Make connection to gRPC server */
	cc, err := grpc.Dial("localhost:5001", grpc.WithInsecure())

	/* Handle Error and exit if err occured during connection */
	if err != nil {
		log.Fatalf("Could not connect : %v", err)
		os.Exit(1)
	}

	/* close server at the end of the function (all async tasks) */
	defer cc.Close()

	/* get gRPC client from connection */
	client := pb.NewBibirectionalStreamServiceClient(cc)

	names := []string{"Name One", "Name Two", "Name Three"}

	stream, _ := client.Message(context.Background())

	/* to lock main go routine on channel */
	channel := make(chan int)

	/* Demo of go routine for streaming requests */
	go func() {

		for _, n := range names {
			/* req */
			req := &pb.Request{
				Name: n,
			}

			stream.Send(req)
		}

		stream.CloseSend()

	}()

	/* Go routine to stream responses */
	go func() {

		for {
			res, err := stream.Recv()
			if err == io.EOF {
				channel <- 0
				break
			}
			if err != nil {
				log.Fatalln("Error : ", err)
				os.Exit(1)
			}

			/* Display result */
			fmt.Println(res.GetMessage())
		}
	}()

	/* wait for channel */
	<-channel

}
