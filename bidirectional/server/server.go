package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"gitlab.com/dig_zap/grpc-poc/bidirectional/pb"
	"google.golang.org/grpc"
)

/* declare server type */
type server struct{}

func (*server) Message(stream pb.BibirectionalStreamService_MessageServer) error {
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalln("Error ", err)
			os.Exit(1)
		}

		res := &pb.Response{
			Message: "Hello! " + req.GetName(),
		}

		stream.Send(res)
	}

	return nil
}

func main() {

	fmt.Println("Bidirectional streaming server starting!")

	/* Start tcp server on port 5001 */
	lis, err := net.Listen("tcp", "0.0.0.0:5001")

	/* Handle Error and exit if err occured during binding to port */
	if err != nil {
		log.Fatalf("Error : %v", err)
		os.Exit(1)
	}

	/* Create New gRCP server */
	s := grpc.NewServer()

	/* Register server type with gRPC */
	pb.RegisterBibirectionalStreamServiceServer(s, &server{})

	/* Throw erroe if gRPC can't serve on TCP server */
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
