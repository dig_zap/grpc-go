package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/dig_zap/grpc-poc/client_streaming/pb"
	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Client for streaming server starting!")

	/* Make connection to gRPC server */
	cc, err := grpc.Dial("localhost:5001", grpc.WithInsecure())

	/* Handle Error and exit if err occured during connection */
	if err != nil {
		log.Fatalf("Could not connect : %v", err)
		os.Exit(1)
	}

	/* close server at the end of the function (all async tasks) */
	defer cc.Close()

	/* get gRPC client from connection */
	client := pb.NewClientStreamServiceClient(cc)

	/* Get Stream */
	stream, _ := client.Sum(context.Background())

	/* Loop from 1 to 10 */
	for i := 1; i < 11; i++ {

		/* Build request */
		req := &pb.Request{
			Number: int32(i), // any number to square
		}

		stream.Send(req)
	}

	/* Get result */
	res, _ := stream.CloseAndRecv()

	/* Display result */
	fmt.Printf("Sum of is %d\n", res.GetResult())
}
