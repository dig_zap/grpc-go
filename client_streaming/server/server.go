package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"gitlab.com/dig_zap/grpc-poc/client_streaming/pb"

	"google.golang.org/grpc"
)

/* declare server type */
type server struct{}

func (*server) Sum(stream pb.ClientStreamService_SumServer) error {

	fmt.Println("Sum has been invoked")

	var sum int32 = 0

	for {
		req, err := stream.Recv()
		if err == io.EOF {

			res := &pb.Response{
				Result: sum,
			}

			return stream.SendAndClose(res)
		}
		if err != nil {
			log.Fatalln("Error : ", err)
			os.Exit(1)
		}

		sum = sum + req.GetNumber()
	}
}

func main() {

	fmt.Println("Streaming server starting!")

	/* Start tcp server on port 5001 */
	lis, err := net.Listen("tcp", "0.0.0.0:5001")

	/* Handle Error and exit if err occured during binding to port */
	if err != nil {
		log.Fatalf("Error : %v", err)
		os.Exit(1)
	}

	/* Create New gRCP server */
	s := grpc.NewServer()

	/* Register server type with gRPC */
	pb.RegisterClientStreamServiceServer(s, &server{})

	/* Throw erroe if gRPC can't serve on TCP server */
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
