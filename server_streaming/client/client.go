package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/dig_zap/grpc-poc/server_streaming/pb"
	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Client for streaming server starting!")

	/* Make connection to gRPC server */
	cc, err := grpc.Dial("localhost:5001", grpc.WithInsecure())

	/* Handle Error and exit if err occured during connection */
	if err != nil {
		log.Fatalf("Could not connect : %v", err)
		os.Exit(1)
	}

	/* close server at the end of the function (all async tasks) */
	defer cc.Close()

	/* get gRPC client from connection */
	client := pb.NewServerStreamServiceClient(cc)

	/* req */
	req := &pb.Request{
		Text: "This is a great day",
	}

	/* Get Stream */
	stream, _ := client.Split(context.Background(), req)

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalln("Error : ", err)
			os.Exit(1)
		}

		/* Display result */
		fmt.Println(res.GetCharacter())
	}

}
