package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/dig_zap/grpc-poc/server_streaming/pb"
	"google.golang.org/grpc"
)

/* declare server type */
type server struct{}

func (*server) Split(req *pb.Request, stream pb.ServerStreamService_SplitServer) error {

	fmt.Println("Split has been invoked")

	text := req.GetText()

	for _, c := range text {
		res := &pb.Response{
			Character: string(c),
		}
		stream.Send(res)
	}

	return nil
}

func main() {

	fmt.Println("Streaming server starting!")

	/* Start tcp server on port 5001 */
	lis, err := net.Listen("tcp", "0.0.0.0:5001")

	/* Handle Error and exit if err occured during binding to port */
	if err != nil {
		log.Fatalf("Error : %v", err)
		os.Exit(1)
	}

	/* Create New gRCP server */
	s := grpc.NewServer()

	/* Register server type with gRPC */
	pb.RegisterServerStreamServiceServer(s, &server{})

	/* Throw erroe if gRPC can't serve on TCP server */
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
