package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/dig_zap/grpc-poc/unary/pb"

	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Unary client starting!")

	/* Make connection to gRPC server */
	cc, err := grpc.Dial("localhost:5001", grpc.WithInsecure())

	/* Handle Error and exit if err occured during connection */
	if err != nil {
		log.Fatalf("Could not connect : %v", err)
		os.Exit(1)
	}

	/* close server at the end of the function (all async tasks) */
	defer cc.Close()

	/* get gRPC client from connection */
	client := pb.NewSquareServiceClient(cc)

	/* Build request */
	req := &pb.Request{
		Number: 2, // any number to square
	}

	/* Get computer square from server */
	result, _ := client.Suqare(context.Background(), req)

	/* Display result */

	fmt.Printf("Suqare of %d is %d\n", result.GetResult().GetNumber(), result.GetResult().GetComputerSquare())

}
