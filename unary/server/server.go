package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/dig_zap/grpc-poc/unary/pb"

	"google.golang.org/grpc"
)

/* declare server type */
type server struct{}

func (s server) Suqare(ctx context.Context, req *pb.Request) (*pb.Response, error) {

	fmt.Println("Suqare was invoked with ", req.GetNumber())

	number := req.GetNumber()

	computedSquare := number * number

	res := pb.Response{
		Result: &pb.NumberAndSquare{
			Number:         number,
			ComputerSquare: computedSquare,
		},
	}
	return &res, nil
}

func main() {

	fmt.Println("Unary server starting!")

	/* Start tcp server on port 5001 */
	lis, err := net.Listen("tcp", "0.0.0.0:5001")

	/* Handle Error and exit if err occured during binding to port */
	if err != nil {
		log.Fatalf("Error : %v", err)
		os.Exit(1)
	}

	/* Create New gRCP server */
	s := grpc.NewServer()

	/* Register server type with gRPC */
	pb.RegisterSquareServiceServer(s, &server{})

	/* Throw erroe if gRPC can't serve on TCP server */
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
